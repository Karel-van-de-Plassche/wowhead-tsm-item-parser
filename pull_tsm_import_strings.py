import requests
from IPython import embed
from html.parser import HTMLParser
from ast import literal_eval
import logging
from collections import UserDict

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

EXPANSION_FILTER_NUM = 166
PROFESSION_FILTER_NUM = 86

# Professions list and definitions from https://wowpedia.fandom.com/wiki/Profession
# Primary professions
PRIMARY_GATHERING_PROFESSION_LIST = [
    "Herbalism",  # Uses different filter "70;1;0" Obtained through herb gathering
    "Mining",  # Uses different filter "73;1;0" Obtained through mining
    "Skinning",  # Uses different filter "76;1;0" Obtained through skinning
]

PRIMARY_PROFESSION_LIST = [
    "Alchemy",
    "Blacksmithing",
    "Enchanting",
    "Engineering",
    "Inscription",
    "Jewelcrafting",
    "Leatherworking",
    "Tailoring",
]


# Secondary professions
# Cooking 99;3;0
SECONDARY_PROFESSION_LIST = [
    "Archaeology", #16
    "Cooking", #3
    "Fishing", #13
    "First Aid",
]


def build_expansion_filter(expansion_name: str) -> tuple:
    if expansion_name in EXPANSION_MAP:
        num = EXPANSION_MAP[expansion_name]
    else:
        raise Exception(f"Unknown expansion name {expansion_name}")
    return (EXPANSION_FILTER_NUM, num, 0)

EXPANSION_MAP = {
    "DF": 10,
    "SL": 9,
    "BFA": 8,
}

GATHERING_PROFESSION_MAP = {
    "Herbalism": 70,
    "Mining": 73,
    "Skinning": 76,
}
PROFESSION_MAP = {
    "Alchemy": 1,
    "Archeology": 16,
    "Blacksmithing": 2,
    "Enchanting": 4,
    "Engineering": 5,
    "Inscription": 15,
    "Jewelcrafting": 7,
    "Leatherworking": 8,
    "Tailoring": 10,
}
def build_profession_filter(profession_name: str) -> tuple:
    if profession_name in PRIMARY_PROFESSION_LIST:
        num0 = PROFESSION_FILTER_NUM
        num1 = PROFESSION_MAP[profession_name]
    elif profession_name in PRIMARY_GATHERING_PROFESSION_LIST:
        num0 = GATHERING_PROFESSION_MAP[profession_name]
        num1 = 1
    else:
        raise Exception(f"Unknown profession name {profession_name}")

    assert num0 != -1
    assert num1 != -1

    return (num0, num1, 0)


def build_filter_str(filters: list) -> str:
    filter_str = ""
    for ii in range(3):
        filter_str += ":".join(str(fil[ii]) for fil in filters)
        if ii != 2:
            filter_str += ";"
    return filter_str


WOWHEAD_URL = "https://www.wowhead.com/"
WOWHEAD_FILTERED_ITEM_URI = "items?filter="


def is_id(attr, qid):
    print(attr)
    return attr[0] == "id" and attr[1] == qid


ITEM_LIST_PRESTRING = "//<![CDATA[\nWH.Gatherer.addData("
# Filter HTTPS query example: Shadowlands https://www.wowhead.com/items?filter=166;9;0
# Returned data looks something like
# data[len(ITEM_LIST_PRESTRING) :][:600]
# '3, 1, {"190189":{"name_enus":"Sandworn Relic","quality":4,"icon":"creatureportrait_altarofair_01","screenshot":{},"jsonequip":{"reqlevel":60},"attainable":0,"flags2":8192},
#        "189863":{"name_enus":"Spatial Opener","quality":3,"icon":"spell_progenitor_orb","screenshot":{},"jsonequip":{"maxcount":7,"reqlevel":1},"attainable":0,"flags2":8192},
#        "23705":{"name_enus":"Tabard of Flame","quality":4,"icon":"inv_misc_tabardpvp_02","screenshot":{"id":551460,"imageType":2},"jsonequip":{"appearances":{"0":[37058,""]},"displayid":37058,"slotbak":19},"attainable":0,"flags2":8192},"189862":{"name_enus":"Gavel of '.....
# Which we can interpret asa a dict, mostly..
# Other than the in-browser version, this seems to return all items at once
class WowheadFilteredItemParser(HTMLParser):
    itemdict = {}

    def handle_data(self, data):
        if data.startswith(ITEM_LIST_PRESTRING):
            to_be_parsed = data[len(ITEM_LIST_PRESTRING) :]
            ii = to_be_parsed.find("{")
            itemdict = to_be_parsed[ii:]
            itemdict = itemdict[: itemdict.rfind("\nvar") - 2].strip()
            itemdict = itemdict.splitlines()[0]
            # This is different for gather professions
            is_gather_profession = itemdict[-1] == "}" and itemdict[-2] == "}"
            if not is_gather_profession:
                itemdict = itemdict[:-2]
            # I do not think there is a reliable way to check if we got all
            # items requested. From manual checks looks fine
            self.itemdict = literal_eval(itemdict)
            logger.info("Parsed data")

class Item(UserDict):
    """ A WoW in-game item """
    def __init__(self, sid, *args, **kwargs):
        super().__init__(*args, **kwargs)


def pull_for_profession(exp: str, prof: str):
    filters = [
        build_expansion_filter(exp),
        build_profession_filter(prof),
    ]
    filter_str = build_filter_str(filters)
    get_url = f"{WOWHEAD_URL}{WOWHEAD_FILTERED_ITEM_URI}{filter_str}"
    rr = requests.get(get_url)
    logger.info(f"Getting %s", get_url)
    if rr.status_code == 200:
        parser = WowheadFilteredItemParser()
        parser.feed(rr.text)
        if len(parser.itemdict) > 0:
            logger.info("Found %s items for profession '%s'", len(parser.itemdict), prof)
        else:
            logger.info("Found no items for profession '%s'", prof)
            return

        # Example of an Item Description
        #{'name_enus': 'Torch Burst',
        # 'quality': 2,
        # 'icon': 'inv_alchemy_90_modifiedreagent_orange',
        # 'screenshot': {},
        # 'jsonequip': {'cooldown': 180000, 'reqlevel': 1},
        # 'attainable': 0,
        # 'flags2': 8192}

        for sid, item_descr in parser.itemdict.items():
            item: Item = Item(sid, item_descr)
            assert "name_enus" in item
            assert "flags2" in item
        sids = list(parser.itemdict.keys())
        ITEM_PREFIX = "i"
        ITEM_SEPARATOR = ":"
        LIST_SEPARATOR = ","
        lst = [f"{ITEM_PREFIX}{ITEM_SEPARATOR}{ss}{LIST_SEPARATOR}" for ss in sids]
        lst[-1] = lst[-1][:-len(LIST_SEPARATOR)] # Remove last comma
        print(f"{prof}: `{''.join(lst)}`")
    else:
        raise Exception(
            f"Getting url '{get_url}' failed, skipping {prof} and continuing.."
        )


if __name__ == "__main__":
    exp = "DF"
    for prof in PRIMARY_GATHERING_PROFESSION_LIST + PRIMARY_PROFESSION_LIST:
        pull_for_profession(exp, prof)
